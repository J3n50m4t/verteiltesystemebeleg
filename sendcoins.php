<?php
//PHP Include des RPC Clienten fuer Bitcoind
include 'function.php';
?>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
    <script type="text/javascript" src="http://netdna.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
    <link href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet"
          type="text/css">
    <link href="css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
<!-- Header wird eingebunden und geladen -->
<?php include 'header.php'; ?>
<div class="section" id="wallet">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>Versenden von Bitcoins</h1>
                <p contenteditable="true">Aktuelle Balance: <?php echo $rpcconnection->getbalance(); ?></p>
                <!--
                Hier gibt es nur ein Formular das eine zweite Seite aufruft
                -->
                <form action="sendcoinsdone.php" method="post">
                    <p>Adresse: <input style="width:80%; float:right" type="text" name="adressezuder" required/></p>
                    <p>Anzahl der Coins<input style="width:80%; float:right" type="text" name="anzahldercoins"
                                              required/></p>
                    <p><input class="btn btn-primary btn-width80" type="submit"/></p>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Footer wird eingebunden und geladen -->
<?php include 'footer.php'; ?>
</body>
</html>

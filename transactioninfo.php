<?php
//PHP Include des RPC Clienten fuer Bitcoind
include 'function.php';
function keineInformationen()
{
    //Ausgabe, dass die aktuell gewuenschte Information noch nicht enthalten ist
    echo "Zur Zeit keine Informationen<br/>";
}

?>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
    <script type="text/javascript" src="http://netdna.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
    <link href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet"
          type="text/css">
    <link href="css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
<!-- Header wird eingebunden und geladen -->
<?php include 'header.php'; ?>
<div class="section" id="wallet">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>Informationen zur Transaktion</h1>
                <?php
                //Prüft ob ein Wert über URL Parameter übergeben wird
                if (empty($_GET['transactioninfo'])) {
                    ?>
                    <!--
                        Ist kein Wert übergeben worden, so wird ein Formfenster zu  eingeben einer TransaktionsID angezeigt
                        Dieses öffnet erneut die selbe Seite, nur mit einem Parameter in der URL
                        -->
                    <form action="transactioninfo.php?transactioninfo=<?php $_GET['transactioninfo]']; ?> method="
                          post">
                    <p>Transaktion: <input style="width:80%; float:right" type="text" name="transactioninfo"/></p>
                    <p><input class="btn btn-primary  btn-width80" type="submit"/></p>
                    </form>
                    <?php
                } else {
                    //Falls die URL ein Parameter hatte, so wird der Wert in eine Variable geschrieben
                    $transaction = $_GET['transactioninfo'];
                    /*
                     * Es wird versucht die Transactionsinformationen ueber einen
                     * Api-Aufruf zu bekommen
                     * Schlaegt dieser fehl, so gibt es eine entsprechende Ausgabe
                     * und es wird eine Variable gesetzt die die transaktion fue unglueltig erklaert,
                     * damit spaeter die ausgabe nicht erfolgt
                     */
                    try {
                        $response = file_get_contents("https://blockexplorer.com/api/tx/$transaction");
                        if (empty($response)) {
                            throw new Exception("Leider gibt es zu dieser Transaktion gibt es gerade keine Informationen oder die Transaktion exisitiert nicht.<br/>");
                        }
                    } //catch exception
                    catch (Exception $e) {
                        echo $e->getMessage();
                        $transaktionungueltig = true;
                    }
                    //Die empfangenen Daten werden in ein Arrayformat kodiert
                    $jsonr = json_decode($response, true);
                    //Beginn der Ausgabe
                    echo "<span class='text-focus'>Transaktionshash:&emsp;  </span>" . $transaction . "<br/>";
                    // Wenn die Ausgabe gueltig ist, dann wird es ausgefuehrt
                    if ($transaktionungueltig != true) {
                        $inputarraylaenge = count($jsonr['vin']);
                        $value = 0;
                        //Fuer die Anzahl der Einkommenden Transaktionen werden die Summen addiert
                        for ($inputcounter = 0; $inputcounter < $inputarraylaenge; $inputcounter++) {
                            if (key_exists("value", $jsonr['vin'][$inputcounter])) {
                                $value = $value + $jsonr['vin'][$inputcounter]['value'];
                            } else {
                                $value = "Neu erzeugter Coin";
                            }
                        }
                        echo "<span class='text-focus'>Gesamter Input:  &emsp;&emsp;</span>" . $value . "<br/>";
                        $outputarraylaenge = count($jsonr['vout']);
                        $outvalue = 0;
                        //Fuer die Anzahl der Einkommenden Transaktionen werden die Summen addiert
                        for ($outputcounter = 0; $outputcounter < $outputarraylaenge; $outputcounter++) {
                            $outvalue = $outvalue + $jsonr['vout'][$outputcounter]['value'];
                        }
                        echo "<span class='text-focus'>Gesamter Output:  &emsp;</span>" . $outvalue . "<br/>";
                        // Gebuehren ergeben sich aus der einkommenden und ausgehenden Transaktionen
                        $fee = $value - $outvalue;
                        echo "<span class='text-focus'>Gebuehr:  &emsp;&emsp;&emsp;&emsp;&emsp;</span>" . $fee . "<br/>";
                        $blockindex = $rpcconnection->getblock($jsonr['blockhash']);
                        echo "<span class='text-focus'>Blockhash:&emsp;&emsp;&emsp;&emsp;</span>" . "<a href='blockinfo.php?blockinfo=" . $blockindex['height'] . "'>" . $jsonr['blockhash'] . "</a>" . "<br/>";
                        /*
                        echo "<pre>\n";
                        print_r($blockindex);
                        echo "</pre>";
                        */
                    }
                }
                ?>
                <!-- Ende der Ausgabe bei gesuchter transaktion -->
                <hr>
                <br>
                <table class="table-bordered table-condensed">
                    <?php
                    //die letzten 100 Transaktionen in ein Array laden
                    //Parameter: Alle Accounts, 100 Transaktionen
                    $alltransactions = $rpcconnection->listtransactions('*', 100);
                    //Größe des aktuellen Arrays abrufen und speichern für Schleife
                    $max = sizeof($alltransactions);
                    echo "<tr>";
                    echo "<th>Adresse an die gesendet wurde</th> <th>Kategorie</th> <th>Anzahl</th> <th>Anzahl d. Bestätigungen</th> <th> Transaktionsid </th>";
                    //Schleife rückwärts ausführen um neuste Transaktionen zuerst anzuzeigen - Diese stehen im Array an letzter Stelle
                    for ($i = $max - 1; $i >= 0; $i--) {
                        echo "<tr>";
                        //Array aus dem aktuellen Array auslösen
                        $zwischenspeicherarray = $alltransactions[$i];
                        //Verschiedene Werte aus dem neuen Array auslesen und in die Tabelle ausgeben.
                        echo "<td>";
                        echo $zwischenspeicherarray['address'];
                        echo "</td>";
                        $kategorie = $zwischenspeicherarray['category'];
                        if ($kategorie == "send") {
                            echo "<td class='redfont'>";
                            echo $kategorie;
                            echo "</td>";
                        } else {
                            echo "<td class='greenfont'>";
                            echo $kategorie;
                            echo "</td>";
                        }
                        echo "<td>";
                        echo $zwischenspeicherarray['amount'];
                        echo "</td>";
                        echo "<td>";
                        echo $zwischenspeicherarray['confirmations'];
                        echo "</td>";
                        echo "<td>";
                        echo "<a href='transactioninfo.php?transactioninfo=" . $zwischenspeicherarray['txid'] . "'>" . $zwischenspeicherarray['txid'] . "</a>";
                        echo "</td>";
                        echo "</tr>";
                    }
                    ?>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- Footer wird eingebunden und geladen -->
<?php include 'footer.php'; ?>
</body>
</html>

<!--
# Ausgelagerter Teil des Footers
# ermöglicht einfaches includieren auf mehreren Unterseiten bei einfach bleibenden Änderungen
-->
<footer class="section section-primary">
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <h1>Projekt der HSMW</h1>
                <p>Der Client für das Bitcoin Netzwerk wurde von Markus Dittrich im Rahmen
                    einer Belegarbeit für den Studiengang Verteile Systeme angefertigt.
                    <br>
                    <br>Während der Entwicklung&nbsp;verwendete Programme: <a href="http://atom.io/" target="_blank" class="footer">Atom</a>, <a
                        href="http://windows.php.net/download/" target="_blank" class="footer">php7</a>, <a
                        href="https://vivaldi.com" target="_blank" class="footer">Vivaldi</a><br/>
                    Verwendete Tools:
                    <a href="http://getbootstrap.com/" target="_blank" class="footer">Bootstrap</a>,
                    <a href="http://www.jsonrpc.org/" target="_blank" class="footer">jsonrpc</a>,
                    <a href="https://blockexplorer.com/" target="_blank" class="footer">BlockexplorerAPI</a>,
                    <a href="https://bitbucket.org/" target="_blank" class="footer">BitBucket</a>,
                    <a href="http://livereload.com/" target="_blank" class="footer">LiveReload</a>
                </p>
            </div>
            <div class="col-sm-6">
                <p class="text-info text-right">
                    <br>
                    <br>
                </p>

                <div class="row">
                    <div class="col-md-12 hidden-xs text-right">
                        <a href="https://bitbucket.org/J3n50m4t/verteiltesystemebeleg" target="_blank"><i
                                class="fa fa-3x fa-fw fa-bitbucket text-inverse"></i></a>
                        <a href="#top"><i class="fa fa-3x fa-fw fa-arrow-up text-inverse"></i></a>
                        <p>
                            <!--Outout im Footer ob man sich im Testnet oder im BitcoinNet befindet-->
                            <?php
                            $infoarray = $rpcconnection->getinfo();
                            if ($infoarray['testnet'] == 1) {
                                echo "TestNetMode";
                            } elseif ($infoarray['testnet'] == false) {
                                echo "BitcoinMode";
                            }
                            echo '<br/> Aktuelle gesamt Balance: ' . $rpcconnection->getbalance() . '<br />' . "\n";
                            ?>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- Local live reload script -->
<script>document.write('<script src="http://' + (location.host || 'localhost').split(':')[0] + ':35729/livereload.js?snipver=1"></' + 'script>')</script>
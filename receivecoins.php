<?php
//PHP Include des RPC Clienten fuer Bitcoind
include 'function.php';
//Funktion die aufgerufen wird um eine neu Adresse zum empfangen zu erzeugen
function getnewaddress()
{
    //globale variable laden;
    global $account;
    global $rpcconnection;
    //rpc command für neue Adresse
    $rpcconnection->getnewaddress((string)$account);
}

//gibt es das URL Parameter - falls ja ausfuehren falls nicht nichts tun
if (isset($_GET['getnewaddress'])) {
    getnewaddress();
}
?>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
    <script type="text/javascript" src="http://netdna.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
    <link href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet"
          type="text/css">
    <link href="css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
<!-- Header wird eingebunden und geladen -->
<?php include 'header.php'; ?>
<div class="section" id="wallet">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>Empfangen von Bitcoins</h1>
                <p contenteditable="true">Wenn sie bitcoins empfangen wollen können sie das aktuell über folgende
                    Adressen tun.</p>
                <?php
                //Alle Adressen des Accounts in ein Array laden
                $addressenarray = ($rpcconnection->getaddressesbyaccount((string)$account));
                //Groesse des Arrays laden
                $addressarraysize = count($addressenarray);
                //Falls das Array leer ist wird eine Adresse erzeugt
                if ($addressarraysize == 0) {
                    echo "Leider hatten sie noch keine Adresse - wir haben eine Adresse f&uuml;r sie erzeugt<br/>";
                    echo($rpcconnection->getaccountaddress((string)$account));
                } else {
                    //Ansonsten wird ausgegeben wie viele Adressen man hat ...
                    echo "Sie haben aktuell " . $addressarraysize;
                    if ($addressarraysize == 1) {
                        echo " Adresse<br/>";
                    } else {
                        echo " Adressen<br/>";
                    }
                    // ... und die Adresse ausgegeben
                    for ($i = 0; $i < $addressarraysize; $i++) {
                        echo $addressenarray[$i];
                        echo "<br/>";
                    }
                }
                ?>
                <br/>
                Falls sie eine weitere Adresse erzeugen wollen klicken sie hier
                <!--
                    CODE NICHT DIREKT EINBINDEN - Wird ausgeführt auch ohne klick
                    Seite neu aufrufen mit Parameter zum erzeugen einer neuen Adresse
                -->
                <button onclick="window.location.href='receivecoins.php?getnewaddress=true'">
                    Gib mir eine neue Addresse
                </button>
            </div>
        </div>
    </div>
</div>
<!-- Footer wird eingebunden und geladen -->
<?php include 'footer.php'; ?>
</body>
</html>
<?php
//PHP Include des RPC Clienten fuer Bitcoind
include 'function.php';
?>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
    <script type="text/javascript" src="http://netdna.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
    <link href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet"
          type="text/css">
    <link href="css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
<!-- Header wird eingebunden und geladen -->
<?php include 'header.php'; ?>
<div class="section" id="wallet">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>Sie haben Bitcoins verschickt</h1>
                <?php
                //Auslesen der Werte Addresse und Anzahl, die in der sencoins.php als Formular abgeschickt werden
                $adresse = $_POST['adressezuder'];
                $anzahl = $_POST['anzahldercoins'];
                //Ueberpruefen ob die Adresse existiert bzw korrekt ist
                $validierungsarray = $rpcconnection->validateaddress($adresse);
                if ($validierungsarray['isvalid'] == 1) {
                    //Ausgabe, wieviel wohin verschickt wurde
                    echo "Es wurden <b>" . $anzahl . "</b> Bitcoins an die Adresse <b>" . $adresse . "</b> verschickt.<br/>";
                    //Senden der Coins über das RPC
                    $transaction = $rpcconnection->sendtoaddress($adresse, $anzahl);
                    echo "Die ID der Transaktion ist: ";
                    //Ausgabe diverser Informationen zur erstellten Transaktion
                    print_r($transaction);
                    echo "<br/>Weitere Informationen zur Transaktion: ";
                    $transactioninformations = $rpcconnection->gettransaction((string)$transaction);
                    echo "<br/>Gebuehr:&nbsp;" . $transactioninformations['fee'];
                    echo "<br/>Best&auml;tigungen:&nbsp;" . $transactioninformations['confirmations'];
                    echo "<br/>Vertraut:&nbsp;" . $transactioninformations['trusted'];
                    echo "<br/>HEX:&nbsp;" . $transactioninformations['hex'];
                    echo "<br/><a href='transactioninfo.php?transactioninfo=$transaction'>Alle Informationen zur Transaktion</a>";
                    echo "Ihre neue Balance betr&auml;gt:" . $rpcconnection->getbalance();
                    echo "<br/><br/><pre>\n";
                    print_r($rpcconnection->gettransaction((string)$transaction));
                    echo "</pre>";
                } else {
                    echo "Die Adresse ist leider nicht korrekt. ";
                    echo "Ihre Balance betr&auml;gt immer noch:" . $rpcconnection->getbalance();
                }
                ?>
            </div>
        </div>
    </div>
</div>
<!-- Footer wird eingebunden und geladen -->
<?php include 'footer.php'; ?>
</body>
</html>

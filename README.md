# Beleg Markus Dittrich Mi13W3-B
### Der Client
Der Client ist in PHP geschrieben 

Es wird ein Fullnode benötigt, der zu Testzwecken auf meinem Server läuft

DieZugriffsdaten für den Server finden sich in der Function.php und können dort konfiguriert werden

### Benötigte Config Parameter bitcoind

```
testnet=0 // falls gewuenscht
txindex=1
server=1
rpcuser=user    //muss dem der function.php entsprechen
rpcpassword=password //muss dem der function.php entsprechen

```


# Aufgabenstellung Verteilte Systeme
### Rahmenbedingungen
* Einzelarbeit
* Belegabgabe (Quellcode und Dokumentation) bis zum Klausurtermin
* Entweder man gibt den Beleg bis dahin ab oder man nimmt an der Klausur teil
* 30 Minuten Belegverteidigung
    * Live Demo der unten beschriebenen Funktionen
    * Erläuterung des Lösungswegs
    * Beantwortung von Fragen

###Erstellung eines BitcoinClients
* Programmiersprache nach Wahl
* Grafische Oberfläche
* Erstellen einer vollständigen Dokumentation des Quellcodes
* Der Client muss sich mit dem Bitcoinnetzwerk verbinden und auf ankommende Transaktionen und Blöcke lauschen können.
* Visualisierung in Echtzeit der empfangenen:
    * Blöcke: (Blockhash, Nonce, Coinbaseparameter, Blockbelohnung, Anzahl Transaktionen im Block, Größe des Blocks in Byte, IP-Adresse des Knotens von dem man den Block erhalten hat)
    * Transaktionen (Transaktionshash, Summe Inputs in BTC, Summe Outputs in BTC, Gebühren, IP-Adresse des Knotens von dem man die Transaktion erhalten hat)
* Erstellen von neuen Bitcoin-Adressen.
* Erstellen von Signaturen auf beliebige Nachrichten mit den privaten Schlüsseln der Adressen.
* Anzeige des Kontostands.
* Visualisierung der Transaktionen die Bitcoins an die erstellten Adressen senden.
* Erstellen von Transaktionen, die Bitcoins versenden.

### Live Demo bei der Verteidigung
* Demonstration der Visualisierung empfangener Blöcke und Transaktionen.
* Erstellen einer Bitcoinadresse.
* Erstellen einer Signatur mit dieser Adresse auf eine Nachricht, die während der Verteidigung mitgeteilt wird.
* Verifizierung von Signaturen
* Ihnen werden dann Bitcoins an diese Adresse überwiesen:
    * Demonstration der Visualisierung des Zahlungseingangs
	  * Senden der empfangenen Bitcoins an eine weitere Adresse, die ihnen mitgeteilt wird
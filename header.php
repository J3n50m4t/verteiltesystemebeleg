<!--
# Ausgelagerter Teil des Headers
# ermöglicht einfaches includieren auf mehreren Unterseiten bei einfach bleibenden Änderungen
-->
<div class="navbar navbar-default navbar-fixed-top">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-ex-collapse">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand"><span>Bitcoin Client Markus Dittrich</span></a>
    </div>
    <div class="collapse navbar-collapse" id="navbar-ex-collapse top">
      <ul class="nav navbar-nav navbar-right">
        <li>
          <a href="index.php#graph">Home</a>
        </li>
        <li>
          <a href="signandverify.php">Verifizieren</a>
        </li>
        <li>
          <a href="sendcoins.php">Send</a>
        </li>
        <li>
          <a href="transactioninfo.php">Transaktionen</a>
        </li>
        <li>
          <a href="blockinfo.php">Bl&ouml;cke</a>
        </li>
        <li>
          <a href="receivecoins.php">Empfangen</a>
        </li>
      </ul>
    </div>
  </div>
</div>

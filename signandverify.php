<?php
//PHP Include des RPC Clienten fuer Bitcoind
include 'function.php';
//Leere Variablen beretistellen )global)
$signatur = "Noch ist keine Signatur verf&uuml;gbar";
$verified = false;
$adresse = "";
$verifyadresse = "";
$nachricht = "";
$verifynachricht = "";
$verifysignature = "";

function signfunction()
{
    //globale Variablen importieren
    global $rpcconnection;
    global $signatur;
    global $adresse;
    global $nachricht;
    //Variablen aus dem Formformular empfangen
    $adresse = $_POST['adresse'];
    $nachricht = $_POST['nachricht'];
    //Signierung ausfuehren
    $signatur = $rpcconnection->signmessage($adresse, $nachricht);
    //ueberpruefen ob Signatur möglich war
    if (is_array($signatur)) {
        if (key_exists("code", $signatur)) {
            if ($signatur['code'] == "-3")
                $signatur = "Falsche Adresse";
        }
    }
}

function verifyfunction()
{
    //globale Variablen importieren
    global $rpcconnection;
    global $verifyadresse;
    global $verifynachricht;
    global $verifysignature;
    global $verified;
    //Variablen aus dem Formformular empfangen
    $verifyadresse = $_POST['verifyadresse'];
    $verifynachricht = $_POST['verifynachricht'];
    $verifysignature = $_POST['verifysignature'];
    $verified = $rpcconnection->verifymessage($verifyadresse, $verifysignature, $verifynachricht);
    //ueberpruefen ob Verifizierung möglich war
    if (is_array($verified)) {
        if (key_exists("code", $verified)) {
            if ($verified['code'] == "-5")
                $verified = "Falsche Adresse";
        }
    }
}

//Falls Formular Sign/verify abgeschickt wird befindet sich in den URL Parametern ein sign/verify-> Funktion wird ausgefuehrt
if (isset($_GET['sign'])) {
    signfunction();
}
if (isset($_GET['verify'])) {
    verifyfunction();
}
?>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
    <script type="text/javascript" src="http://netdna.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
    <link href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet"
          type="text/css">
    <link href="css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
<!-- Header wird eingebunden und geladen -->
<?php include 'header.php'; ?>
<div class="section" id="wallet">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>Signieren und Pr&uuml;fen von Nachrichten</h1>
                <div class="col-md-6">
                    <h1>Verschl&uuml;sseln</h1>
                    <br/>
                    <!-- Adresse und Nachricht werden aus den entsprechenden Feldern geladen und dann beim abschicken bereitgestellt -->
                    <form action="signandverify.php?sign=true" method="post">
                        <p>Adresse: <input style="width:80%; float:right" type="text" name="adresse" required value="<?php
                            global $adresse;
                            echo $adresse; ?>"/></p>
                        <p>Nachricht: <input style="width:80%; float:right" type="text" name="nachricht" required value="<?php
                            global $nachricht;
                            echo $nachricht; ?>"/></p>
                        <p><input class="btn btn-primary btn-width80" type="submit"/></p>
                    </form>
                    <input
                        style="width:80%; float:right"
                        type="text"
                        name="name3"
                        size="25"
                        maxlength="50"
                        value="<?php global $signatur;
                        echo (string)$signatur; ?>" disabled>
                </div>
                <div class="col-md-6">
                    <h1>Pr&uuml;fen</h1>
                    <br/>
                    <form action="signandverify.php?verify=true" method="post">
                        <!-- Adresse Nachricht und Signatur werden aus den entsprechenden Feldern geladen und dann beim abschicken bereitgestellt -->
                        <p>Adresse: <input style="width:80%; float:right" type="text" name="verifyadresse" required value="<?php
                            global $verifyadresse;
                            echo $verifyadresse; ?>"/></p>
                        <p>Nachricht: <input style="width:80%; float:right" type="text" name="verifynachricht" required value="<?php
                            global $verifynachricht;
                            echo $verifynachricht; ?>"/></p>
                        <p>Signatur: <input style="width:80%; float:right" type="text" name="verifysignature" required value="<?php
                            global $verifysignature;
                            echo $verifysignature; ?>"/></p>
                        <p><input class="btn btn-primary btn-width80" type="submit"/></p>
                    </form>
                    <input
                        style="width:80%; float:right"
                        type="text"
                        name="name3"
                        size="25"
                        maxlength="50"
                        value="<?php global $verified;
                        print_r($verified); ?>" disabled>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Footer wird eingebunden und geladen -->
<?php include 'footer.php'; ?>
</body>
</html>
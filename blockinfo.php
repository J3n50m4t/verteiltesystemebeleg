<?php
//PHP Include des RPC Clienten fuer Bitcoind
include 'function.php';
?>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
    <script type="text/javascript" src="http://netdna.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
    <link href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet"
          type="text/css">
    <link href="css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
<!-- Header wird eingebunden und geladen -->
<?php include 'header.php'; ?>
<div class="section" id="wallet">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>Informationen zum Block</h1>
                <?php
                //Prüft ob ein Wert über URL Parameter übergeben wird
                if (empty($_GET['blockinfo'])) {
                    ?>
                    <!--
                        Ist kein Wert übergeben worden, so wird ein Formfenster zu  eingeben einer TransaktionsID angezeigt
                        Dieses öffnet erneut die selbe Seite, nur mit einem Parameter in der URL
                    -->
                    <form action="blockinfo.php?blockinfo=<?php $_GET['blockinfo]']; ?> method=" post">
                    <p>Blockindex: <input style="width:80%; float:right" type="text" name="blockinfo"/></p>
                    <p><input class="btn btn-primary  btn-width80" type="submit"/></p>
                    </form>
                    <?php
                } else {
                    $blockindex = $_GET['blockinfo'];
                    try {
                        $blockhash = $rpcconnection->getblockhash((int)$blockindex);
                        if (empty($blockhash)) {
                            throw new Exception("Leider gibt es zu diesem Block gerade keine Informationen oder der Index exisitiert nicht.<br/>");
                        }
                    } //catch exception
                    catch (Exception $e) {
                        echo $e->getMessage();
                        $blockungueltig = true;
                    }
                    if ($blockungueltig != true) {
                        //Array mit den Daten aus dem Node laden, indem wir den hashwert übergeben
                        $blockarray = $rpcconnection->getblock($blockhash);

                        /*
                         * Reward wird auf 50 festgesetzt
                         * Falls Blockindex groesser als Wert, so wird Belohnung angepasst
                         * Werte uebernommen aus
                         * https://en.bitcoin.it/wiki/Controlled_supply#Projected_Bitcoins_Long_Term
                        */
                        $reward = 50;
                        if ($blockindex >= 210000) {
                            $reward = 25;
                            if ($blockindex >= 420000) {
                                $reward = 12.5;
                                if ($blockindex >= 630000) {
                                    $reward = 6.25;
                                }
                            }
                        }
                        /*
                         * Transaktionen sind im Blockarray als weiteres Array gespeichert (tx)
                         * Größe des Arrays = Anzahl der Transaktionen
                         */
                        $transaktionenzahl = sizeof($blockarray['tx']);
                        echo "<h3>Aktueller Block: #" . $blockindex . "</h3>";
                        //Tabelle mit Ausgabe $blockarray['gesuchterWert]
                        echo '<table class="table-bordered table-condensed" width="100%">';
                        echo "<tr>";
                        echo "<th width='20%'>Hash</th><td width='80%'>" . $blockarray['hash'] . "</td>";
                        echo "</tr>";
                        echo "<tr>";
                        echo "<th width='20%'>Bestätigungen</th><td width='80%'>" . $blockarray['confirmations'] . "</td>";
                        echo "</tr>";
                        echo "<tr>";
                        echo "<th width='20%'>Größe in Byte</th><td width='80%'>" . $blockarray['size'] . "</td>";
                        echo "</tr>";
                        echo "<tr>";
                        echo "<th width='20%'>Version</th><td width='80%'>" . $blockarray['version'] . "</td>";
                        echo "</tr>";
                        echo "<tr>";
                        echo "<th width='20%'>Nonce</th><td width='80%'>" . $blockarray['nonce'] . "</td>";
                        echo "</tr>";
                        echo "<tr>";
                        echo "<th width='20%'>Schwierigkeit</th><td width='80%'>" . $blockarray['difficulty'] . "</td>";
                        echo "</tr>";
                        echo "<tr>";
                        echo "<th width='20%'>Blockbelohnung</th><td width='80%'>" . $reward . " BTC" . "</td>";
                        echo "</tr>";
                        echo "<tr>";
                        echo "<th width='20%'>Anzahl der Transaktionen</th><td width='80%'>" . $transaktionenzahl . "</td>";
                        echo "</tr>";
                        echo "</table>";
                        //Für die Ausgabe des Arrays auskommentieren
                        /*
                        echo "<pre>\n";
                        print_r($blockarray);
                        echo "</pre>";
                        */
                    }
                }
                ?>
                <!-- Ende der Ausgabe bei gesuchtem Block -->
                <br>
            </div>
        </div>
    </div>
</div>
<!-- Footer wird eingebunden und geladen -->
<?php include 'footer.php'; ?>
</body>
</html>

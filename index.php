<?php
require_once 'function.php';
?>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
    <script type="text/javascript" src="http://netdna.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
    <link href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet"
          type="text/css">
    <link href="css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
<!-- Header wird eingebunden und geladen -->
<?php include 'header.php'; ?>
<div class="section" id="graph">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h1>Bl&ouml;&opke</h1>
                <?php
                //aktueller Block wird aus dem Knoten ausgelesen und jeweils um entsprechenden wert reduziert um die Vorgaenger zu bekommen
                //schliesslich werden sie ausgegeben

                $latestblock = $rpcconnection->getblockcount();
                if ($latestblock == 0) {
                    //Falls bitcoin node nicht erreichbar werden nun keine Minusbloecke gezeigt
                } else {
                    $latestblock1 = $latestblock - 1;
                    $latestblock2 = $latestblock - 2;
                    $latestblock3 = $latestblock - 3;
                    $latestblock4 = $latestblock - 4;

                    echo "<table class='table-bordered table-condensed' width='100%'>";
                    echo "<tr>";
                    echo "<td>";
                    echo "<a href='blockinfo.php?blockinfo=" . $latestblock . "'>" . $latestblock . "</a>" . "<br/>";
                    echo "</td>";
                    echo "</tr>";
                    echo "<tr>";
                    echo "<td>";
                    echo "<a href='blockinfo.php?blockinfo=" . $latestblock1 . "'>" . $latestblock1 . "</a>" . "<br/>";
                    echo "</td>";
                    echo "</tr>";
                    echo "<tr>";
                    echo "<td>";
                    echo "<a href='blockinfo.php?blockinfo=" . $latestblock2 . "'>" . $latestblock2 . "</a>" . "<br/>";
                    echo "</td>";
                    echo "</tr>";
                    echo "<tr>";
                    echo "<td>";
                    echo "<a href='blockinfo.php?blockinfo=" . $latestblock3 . "'>" . $latestblock3 . "</a>" . "<br/>";
                    echo "</td>";
                    echo "</tr>";
                    echo "<tr>";
                    echo "<td>";
                    echo "<a href='blockinfo.php?blockinfo=" . $latestblock4 . "'>" . $latestblock4 . "</a>" . "<br/>";
                    echo "</td>";
                    echo "</tr>";
                    echo "</table>";
                }
                ?>
            </div>
            <div class="col-md-6">
                <h1>Transaktionen</h1>
                <!--
                Unbestaetigte Transaktionen werden über die websocket api von blockexplorer geladen und dann ausgegeben.
                Es wird eine Tabele bereitgestellt TABELLE in der leere Werte dargestellt werden die dann ueberschrieben werden
                Kommt eine neue Transaktion so wird die jeweils die aelter eine Zeile weiter nach unten geschoben oder sie faellt weg wenn es die letzte war
                -->
                <script src="https://blockexplorer.com/socket.io/socket.io.js"></script>
                <script>
                    //leere Variablen werden erzeugt
                    var neusterHash = "";
                    var tx0 = "";
                    var tx1 = "";
                    var tx2 = "";
                    var tx3 = "";
                    var tx4 = "";
                    var tx5 = "";
                    eventToListenTo = 'tx'
                    room = 'inv'
                    var socket = io("https://blockexplorer.com/");
                    socket.on('connect', function () {
                        // Join the room.
                        socket.emit('subscribe', room);
                    })
                    socket.on(eventToListenTo, function (data) {
                        neusterHash = data.txid;
                        if (tx0 != data.txid) {
                            tx5 = tx4;
                            tx4 = tx3;
                            tx3 = tx2;
                            tx2 = tx1;
                            tx1 = tx0;
                        }
                        document.getElementById("tx1").innerHTML = tx1;
                        document.getElementById("tx2").innerHTML = tx2;
                        document.getElementById("tx3").innerHTML = tx3;
                        document.getElementById("tx4").innerHTML = tx4;
                        document.getElementById("tx5").innerHTML = tx5;
                        tx0 = neusterHash;
                    })
                </script>
                <!-- TABELLE -->
                <table class='table-bordered table-condensed' width='100%'>
                    <tr>
                        <td>
                            <a href="transactioninfo.php"
                               onclick="location.href=this.href+'?transactioninfo='+tx1;return false;"
                               id="tx1"></a><br/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a href="transactioninfo.php"
                               onclick="location.href=this.href+'?transactioninfo='+tx2;return false;"
                               id="tx2"></a><br/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a href="transactioninfo.php"
                               onclick="location.href=this.href+'?transactioninfo='+tx3;return false;"
                               id="tx3"></a><br/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a href="transactioninfo.php"
                               onclick="location.href=this.href+'?transactioninfo='+tx4;return false;"
                               id="tx4"></a><br/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a href="transactioninfo.php"
                               onclick="location.href=this.href+'?transactioninfo='+tx5;return false;"
                               id="tx5"></a><br/>
                        </td>
                    </tr>
                </table>
                <?php
                /*
                 * Falls der Client nicht auf dem aktuellsten Stand ist
                 * Oder er noch nicht bereit ist
                 * gibt er ein Array mit Code -28 zurueck
                 * Hier wird geprueft ob der Code zurueck gegeben wird oder nicht
                 */
                $rpcwarmupcode = ($rpcconnection->getinfo());
                if (key_exists("code", $rpcwarmupcode)) {
                    if ($rpcwarmupcode['code'] == "-28")
                        echo "<p class='errormessage'>Der RPC Client ist noch nicht fertig geladen - bitte warte noch einen kleinen Augenblick</p>";
                }
                ?>
            </div>
        </div>
    </div>
</div>
<div class="section" id="wallet">
    <div class="container">
        <div class="row">
            <div class="col-md-6 square highlighted">
                <h1>Signieren und &Uuml;berpr&uuml;fen</h1>
                <h3>Sign & Verify von Nachrichten </h3>
                <p>Sie m&ouml;chten eine Nachricht verschl&uuml;sseln oder wieder entschl&uuml;sseln?</p>
                <button class="btn btn-primary btn-width80 btn-pgdwn"
                        onclick="window.location.href='signandverify.php'">Pr&uuml;fen
                </button>
                <br/>
            </div>
            <div class="col-md-6 square highlighted">
                <h1>Versenden</h1>
                <h3>Versenden von Bitcoins</h3>
                <p>Hier können Sie Bitcoins verschicken. <br/> Sie haben <?php echo $rpcconnection->getbalance(); ?>
                    Bitcoins zum verschicken</p>
                <button class="btn btn-primary  btn-width80 btn-pgdwn" onclick="window.location.href='sendcoins.php'">
                    Jetzt Bitcoins verschicken
                </button>
                <br/>
            </div>
            <div class="col-md-6 square highlighted">
                <h1>Transaktionsinformation</h1>
                <p>Tragen Sie hier ihre Transaktionsid ein, zu der Sie Informationen wünschen</p>
                <!-- Beim abschicken des Formulars wird die eingegebene Transaktionsid als Parameter an transactioninfo.php übergeben -->
                <form action="transactioninfo.php?transactioninfo=<?php $_GET['transactioninfo]']; ?> method=" post
                ">
                <p>Transaktion: <input style="width:80%; float:right" class="input-sm" type="text"
                                       name="transactioninfo"/></p>
                <p><input class="btn btn-primary  btn-width80 btn-pgdwn" type="submit"/></p>
                </form>
            </div>
            <div class="col-md-6 square highlighted">
                <h1>Empfangen sie Bitcoins</h1>
                <p>Hier finden Sie eine &Uuml;bersicht &uuml;ber alle deine Adressen sowie die M&ouml;glichkeit Neue zu
                    erzeugen</p>
                <button class="btn btn-primary  btn-width80 btn-pgdwn"
                        onclick="window.location.href='receivecoins.php'">Empfangen
                </button>
            </div>
        </div>
    </div>
</div>
<!-- Footer wird eingebunden und geladen -->
<?php include 'footer.php'; ?>
</body>
</html>


